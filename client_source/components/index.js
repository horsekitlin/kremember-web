/**
 * Components Group
 * **/
import Facebook from './facebook';
import Navbar from './navbar';
import Post from './post';
import Datepicker from './datepicker';
import Input from './input';

export default {
    Facebook : Facebook,
    Navbar : Navbar,
    Post : Post,
    Input : Input,
    Datepicker : Datepicker
};
