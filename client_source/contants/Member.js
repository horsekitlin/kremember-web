import keyMirror from 'keymirror'

const Constants = {
    CREATED : '建立記事',
    LIST : '記事列表',
};

export default keyMirror(Constants);
